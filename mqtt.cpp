
#include <ESP8266WiFi.h>

#include "Arduino.h"
#include "mqtt.h"
#include "global.h"
#include "relay.h"
#include "logger.h"
#include <PubSubClient.h>
#include "timer1s.h"
#ifdef  MQTT_MAKE_SECURE
WiFiClientSecure wifiClient;
#else 
WiFiClient wifiClient;
#endif

PubSubClient client;
Relay relay;
extern Timer1s timer;

/**
 * To be called when MQTT message to control releay is received
 */
void mqtt_callback(char* topic, unsigned char* payload, unsigned int length) {
  payload[length] = 0;
  String time = (const char*)payload;
  L_DEBUG("MQTT payload:%s topic:%s",payload,topic);
  if (strncmp(topic, MQTT_CONTROL_TOPIC, strlen(MQTT_CONTROL_TOPIC) - 1) == 0) {
    if (strcmp(topic + strlen(MQTT_CONTROL_TOPIC) - 1, "onFor") == 0) {
      relay.setOffIn(time.toInt());
      relay.setOn();
    }
    if (strcmp(topic + strlen(MQTT_CONTROL_TOPIC) - 1, "offFor") == 0) {
      relay.setOnIn(time.toInt());
      relay.setOff();
    }
  }
}

/**
 * Setup module
 */
void Mqtt::setup() {
  client.setClient(wifiClient);
  client.setServer(MQTT_SERVER_HOST, MQTT_SERVER_PORT);
  client.setCallback(mqtt_callback);
  relay.setup();
}

void Mqtt::loop() {
  static int lastTry;
  if (!client.connected()) {
    int newTry = timer.getTicks();
    if (lastTry != newTry) {
      lastTry = newTry;
      L_NOTICE("Connecting to %s with user %s", MQTT_SERVER_HOST, MQTT_USER);
      if (client.connect(HOST_NAME, MQTT_USER, MQTT_PASSWORD)) {
        client.subscribe(MQTT_CONTROL_TOPIC);
        L_ERROR("Subscribed to %s topic", MQTT_CONTROL_TOPIC);
      } else {
        L_ERROR("Unable to connect to %s with user %s. Reconnectiong in 1s..", HOST_NAME, MQTT_USER);
      }
    }
  } else {
    client.loop();  
  }
  relay.loop();
}

int Mqtt::publish_status(uint8_t* message, size_t length, bool retain) {
  return client.publish(MQTT_STATUS_TOPIC, message, length, retain);
}

int Mqtt::publish_status(const char* message, bool retain) {
  return publish_status((uint8_t*)message, strlen(message), retain);
}

int Mqtt::publish_debug(const char* message, size_t length) {
  return client.publish(MQTT_DEBUG_TOPIC, (uint8_t*)message, length);
}

int Mqtt::publish_debug(uint8_t* message, size_t length) {
  return client.publish(MQTT_DEBUG_TOPIC, message, length);
}

int Mqtt::publish_debug(const char* message) {
  return publish_debug(message, strlen(message));
};

int Mqtt::publish_debug(uint8_t data) {
  char tmp[4];
  return client.publish(MQTT_DEBUG_TOPIC, tmp, 2);
}

int Mqtt::publish(const char* topic, char* message, size_t length, bool retain) {
  return client.publish(topic, (uint8_t*)message, length, retain);
}


