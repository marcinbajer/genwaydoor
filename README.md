# Extension of Genway ECK-03A door control system to work as a part of Elastic based smart building system

Main documentation can be found [here](https://bitbucket.org/marcinbajer/genway_door/raw/master/documentation/Extension%20of%20Genway%20ECK-03A.pdf)

All project configuration can be found in [global.h](global.h) file:

* Specify WiFi login information
* Configure MQTT server connection
* Configure OTA password
* Modify L_LEVEL base on constants specified in [syslog.h](syslog.h) to select log sensitivity

Next [keys.cpp](keys.cpp) file must be modified to include all RfId tags registered in the system