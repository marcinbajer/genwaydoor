#include "logger.h"
#include "Arduino.h"


#ifndef SKIP_MQTT
#include "mqtt.h"
extern Mqtt mqtt;
#endif

/**
 Adds new event to log

 @param level - event severity base on syslog
 @param location - function and class name where event was triggered
 @param file - name of the file and line where event was triggered
 @param format - printf based message to log
*/
void Logger::add(const int level, const char* location, const char* file, const char * format, ...)
{
  va_list argptr;
  va_start(argptr, format);
  storeEvent(level, location, file, format, argptr);
  va_end(argptr);
}

void Logger::storeEvent(const int level, const char* location, const char* file, const char*& format, va_list& argptr) {
  char message[MAX_LOG_ENTRY_SIZE];
  vsnprintf(message, MAX_LOG_ENTRY_SIZE, format, argptr);
#ifdef USE_CONSOLE
  writeToConsole(message, location, file, level);
#endif // USE_CONSOLE
#ifdef USE_MQTT
  writeToMqtt(message, location, file, level);
#endif
#ifdef USE_SYSLOG
  writeToSyslog(message, location, file , level);
#endif // USE_SYSLOG
}

void Logger::prepareEntry( char* buffer, const char * format, ... )
{
  va_list args;
  va_start (args, format);
  vsnprintf(buffer, MAX_LOG_ENTRY_SIZE, format, args);
  va_end (args);
}

#ifdef USE_CONSOLE
void Logger::writeToConsole(const char* message, const char* location, const char* file, const int level) {
  char tmp[MAX_LOG_ENTRY_SIZE];
  prepareEntry(tmp, "%s %s: %s -> %s [%s]\n", getDateAndTime(), getLevel(level), location, message, file);
  Serial.println(tmp);
}
#endif

#ifdef USE_MQTT
void Logger::writeToMqtt(const char* message, const char* location, const char* file, const int level) {
  char tmp[MAX_LOG_ENTRY_SIZE];
  prepareEntry(tmp, "%s:%s() -> %s", getLevel(level), location, message);
  mqtt.publish_debug(tmp);

}
#endif

#ifdef USE_SYSLOG
void Logger::writeToSyslog(const char* message, const char* location, const char* file, const int level) {
  if (message[0] == '{') {
    syslog(level, "{\"location\":\"%s\", \"message\":%s, \"file\":\"%s\", \"application\":\"%s\"}", location, message, file, APPLICATON_NAME);
  } else {
    syslog(level, "{\"location\":\"%s\", \"message\":\"%s\", \"file\":\"%s\", \"application\":\"%s\"}", location, message, file, APPLICATON_NAME);
  } 
}
#endif

/**
 Get current Date and Time
*/
char* Logger::getDateAndTime()
{
  static char result[6];
  static int no;
  sprintf(result, " %d",no++);
  return result;
}

/**
Return event level as text
*/
const char* Logger::getLevel(const int level)
{ 
  const static char* priority[] = { "EMERGENCY", "ALERT", "CRITICAL", "ERROR", "WARNING", "NOTICE", "INFO", "DEBUG" };
  return priority[level];
}

const char* Logger::exeptionToString(const char* message,...) {
  return message;
}


