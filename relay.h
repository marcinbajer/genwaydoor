#pragma once

class Relay
{
  public:
    void setup();
    void setOn();
    void setOff();
    void setOnIn(int seconds);
    void setOffIn(int seconds);
    void loop();
  private:
    volatile int onIn = 0;
    volatile int offIn = 0;

};

