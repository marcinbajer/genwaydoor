#include "keys.h"

const SingleKey keys[] = {
  {4066277,"","Goście",50},
{4066173,"","Goście",49},
{4064804,"Ewa Bajer","Administracja",48},
{4064798,"","Goście",47},
{4064788,"","Goście",46},
{4064773,"","Goście",45},
{4064764,"Janusz Bajer","Administracja",44},
{4064589,"","Administracja",43},
{4063724,"","Administracja",42},
{4063719,"","Administracja",41},
{4063705,"","Administracja",40},
{4063456,"","Administracja",39},
{4062729,"","Administracja",38},
{4062182,"","Administracja",37},
{4061024,"","Mieszkanie 12",36},
{4061017,"","Mieszkanie 12",35},
{4058095,"","Mieszkanie 12",34},
{4057780,"","Mieszkanie 11",33},
{4055147,"","Mieszkanie 11",32},
{4054069,"","Mieszkanie 11",31},
{4053917,"","Mieszkanie 10",30},
{4051161,"","Mieszkanie 10",29},
{4050369,"","Mieszkanie 10",28},
{4050313,"","Mieszkanie 9",27},
{4048979,"","Mieszkanie 9",26},
{4046228,"","Mieszkanie 9",25},
{4046035,"","Mieszkanie 8",24},
{4045768,"","Mieszkanie 8",23},
{4044692,"","Mieszkanie 8",22},
{4044417,"","Mieszkanie 7",21},
{4044413,"","Mieszkanie 7",20},
{4044401,"","Mieszkanie 7",19},
{4044248,"","Mieszkanie 6",18},
{4043769,"","Mieszkanie 6",17},
{4043755,"","Mieszkanie 6",16},
{4043708,"Janusz Bajer","Administracja",15},
{4043361,"","Mieszkanie 5",14},
{4042925,"","Mieszkanie 5",13},
{4042906,"","Mieszkanie 4",12},
{4042852,"","Mieszkanie 4",11},
{4042351,"","Mieszkanie 4",10},
{4041806,"","Mieszkanie 3",9},
{4041803,"","Mieszkanie 3",8},
{4041578,"","Mieszkanie 3",7},
{4041244,"","Mieszkanie 2",6},
{4040943,"","Mieszkanie 2",5},
{4040412,"","Mieszkanie 2",4},
{4040392,"","Mieszkanie 1",3},
{4040370,"","Mieszkanie 1",2},
{4038588,"","Mieszkanie 1",1},
{2780327,"Administracja","Administracja",1000},
{9870826,"Marcin Bajer","Administracja",1001}
};

const int KEYS_NUMBER = sizeof(keys)/sizeof(SingleKey);
