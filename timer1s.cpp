#include "timer1s.h"
#include "Arduino.h"
static volatile long ticks = 0;

timerCallback callback = nullptr;

void inline handler (void) {
  ticks++;
  if (callback != nullptr) callback(ticks);
  timer0_write(ESP.getCycleCount() + 2 * 41660000);
}

void Timer1s::setup() {
  noInterrupts();
  timer0_isr_init();
  timer0_attachInterrupt(handler);
  timer0_write(ESP.getCycleCount() + 2 * 41660000);
  interrupts(); // See more at: http://www.esp8266.com/viewtopic.php?f=8&t=4865#sthash.eogrRK4G.dpuf
}

long Timer1s::getTicks() {
  return ticks;
}

void Timer1s::stop() {
  timer0_detachInterrupt();
}

void Timer1s::setTimerCallback(timerCallback function) {
  callback = function;
}



