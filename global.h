#pragma once
#ifndef GLOBAL_H_
#define GLOBAL_H_

#define WIFI_SSID "YOUR_WIFI_SSID"
#define WIFI_PASSWORD "YOUR_WIFI_PASSWORD"
#define MQTT_SERVER_HOST "YOUR_MQTT_SERVER_IP"

//#define MQTT_MAKE_SECURE
#ifdef  MQTT_MAKE_SECURE
  #define MQTT_SERVER_PORT 8883
#else
  #define MQTT_SERVER_PORT 1883
#endif

#define MQTT_USER "MQTT_USER"
#define MQTT_PASSWORD "MQTT_PASSWORD"
#define HOST_NAME "YOUR_DEVICE_NAME"
#define MQTT_CONTROL_TOPIC "" HOST_NAME "/control/#"
#define MQTT_STATUS_TOPIC "" HOST_NAME "/status"
#define MQTT_DEBUG_TOPIC "" HOST_NAME "/debug"
#define MQTT_OPEN_CODES_TOPIC "" HOST_NAME "/codes"
#define MQTT_RAW_TELEGRAM "" HOST_NAME "/raw"

#define RELAY_PIN 2
#define RELAY_ON 1
#define RELAY_OFF 0

#define MAX_LOG_ENTRY_SIZE 255
#define L_LEVEL LOG_ERR ///< Minimal level of event severity to log base on syslog level
//#define SKIP_MQTT

#define APPLICATON_NAME HOST_NAME
#define OTA_HOSTNAME HOST_NAME 
#define OTA_PASSWORD "OTA_PASSWORD"

#endif


