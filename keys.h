#pragma once
#ifndef KEYS_H_
#define KEYS_H_

typedef struct {
  int code;
  char* owner;
  char* flat;
  int number;
} SingleKey;

extern const SingleKey keys[];
extern const int KEYS_NUMBER;
#endif
