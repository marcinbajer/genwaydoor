#include <stdint.h>
#include "Arduino.h"
#include "relay.h"
#include "logger.h"
#include "global.h"
#include "timer1s.h"

Timer1s timer;

void Relay::setup() {
  setOff();
  timer.setup();
}

/**
 * Loop of module
 * 
 * Decrease if needed \ref onIn and \ref offIn peform off/on relay when time occures
 */
void Relay::loop() {
  static long old_ticks = 0;
  long ticks = timer.getTicks();
  if (ticks != old_ticks) {
    if (onIn != 0) {
      onIn--;
      if (onIn == 0) {
        setOn();
      }
    }
    if (offIn != 0) {
      offIn--;
      if (offIn == 0) {
        setOff();
      }
    }
    old_ticks = ticks;
  }
}

void Relay::setOn() {
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, RELAY_ON);
  L_NOTICE("Relay ON");
}

void Relay::setOff() {
  pinMode(RELAY_PIN, INPUT);
  digitalWrite(RELAY_PIN, RELAY_OFF);
  L_NOTICE("Relay OFF");
}

void Relay::setOnIn(int seconds) {
  onIn = seconds;
  L_NOTICE("Relay will be ON in %d s", seconds);
}

void Relay::setOffIn(int seconds) {
  offIn = seconds;
  L_DEBUG("Relay will be OFF in %d s", seconds);
}



