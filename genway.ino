#include "global.h"
#include <ESP8266WiFi.h>
#include "mqtt.h"
#include "rsgenway.h"
#include "logger.h"
#include "espOTA.h"
#include "espWiFi.h"

#ifndef SKIP_MQTT
Mqtt mqtt;
#endif
EspOTA espOTA;
EspWiFi espWiFi;
RsGenway genway;


/**
 * Add here all initialization function of submodules
 */
void setup()
{
  espWiFi.setup();
#ifndef SKIP_MQTT
  mqtt.setup();
#endif
  genway.setup();
  espOTA.setup();
}

/**
 * Add here all loop functions from submodules
 */
void loop() {
#ifndef SKIP_MQTT
  mqtt.loop();
#endif
  genway.loop();
  espOTA.loop();
}

