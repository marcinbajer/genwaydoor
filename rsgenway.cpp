#include "PubSubClient.h"
#include "rsgenway.h"
#include "mqtt.h"
#include "logger.h"
#include "Arduino.h"
#include "keys.h"

#ifndef SKIP_MQTT
extern Mqtt mqtt;
#endif

/**
 * When RfId tag is detected reader sends telegram via RS-485 interface with bandwith of 2400 \ref 
 * - It starts with sequence of 16 times 0x00 (\ref ZEROS_IN_PREAMBLE)
 * - Start byte of 0xAA (\ref START_CHAR)
 * - 1 Byte of command type (\ref AUTHORNIZED, \ref NOT_AUTHORIZED, \ref PATROL)
 * - Two bytes of station ID (set with F7 via IR remote)
 * - 3 bytes of card id
 * - 3 bytes of zeros (not know purpose) 
 * - 1 byte CRC
 * - 1 byte of ending 0xFE (\ref END_CHAR)
 * 
 * 
 * 
\dot {
digraph g {
graph [
rankdir = "TB"
];
node [
fontsize = "16"
shape = "ellipse"

];
edge [
];
subgraph cluster_1 {
"preamble" [
label = "<f0>0x00|0x00|..|0x00|0x00<f1>"
shape = "record"
];
"start" [
label = "0xAA"
shape = "record"
];
"command" [
label = "0x20"
shape = "record"
];
"stationId" [
label = "0x12|0x34"
shape = "record"
];
"cardId" [
label = "0xAB|0xCD|0xEF"
shape = "record"
];
"postscript" [
label = "0x00|0x00|0x00"
shape = "record"
];
"crc" [
label = "0xAB"
shape = "record"
];
"ending" [
label = "0xFE"
shape = "record"
];
}
"Preamble\n(16 bytes)" [color=white] 
"Preamble\n(16 bytes)" -> preamble

"Start\ncharacter" [color=white]
"Start\ncharacter" -> start

"Command\n byte" [color=white]
"Command\n byte" -> command

"Station\n address" [color=white]
"Station\n address" -> stationId

"Card\n ID" [color=white]
"Card\n ID" -> cardId

"Follow-up" [color=white]
"Follow-up" -> postscript

"CRC" [color=white]
"CRC" -> crc

"Ending\n character" [color=white]
"Ending\n character" -> ending
}
}
 */


#define AUTHORNIZED     0x20
#define NOT_AUTHORIZED  0x22
#define PATROL          0x21 

#define BANDWIDTH 2400

void RsGenway::setup() {
  Serial.begin(BANDWIDTH);
  index = 0;
  zeros = 0;
}

bool RsGenway::detectStart(const uint8_t rec) {
  if (rec == 0) {
    zeros++;
  } else {
    if ((rec == START_CHAR) && (zeros == ZEROS_IN_PREAMBLE)) {
      index = 0;
      zeros = 0;
      return true;
    } else {
      zeros = 0;
      return false;
    }
  }
  return false;
}

bool RsGenway::detectEnd(const uint8_t rec) {
  if ((index == KEY_LENGTH) && (rec == END_CHAR)) {
    return true;
  } else {
    return false;
  };
}

int RsGenway::getCardCodeAsText(char* outBuffer) {
  int code = (buffer[3]<<16)+(buffer[4]<<8)+(buffer[5]);
  int door = (buffer[1]<<8)+(buffer[2]);
  int key = -1;
  outBuffer += sprintf(outBuffer, "{\"code\":%d,\"door\":%d", code,door);
  if (buffer[0]==AUTHORNIZED) {
    outBuffer += sprintf(outBuffer, ",\"valid\":true");
  } else if (buffer[0]==PATROL) {
    outBuffer += sprintf(outBuffer, ",\"patrol\":true");
  } else {
    outBuffer += sprintf(outBuffer, ",\"valid\":false");
  }
  for (int i=0;i<KEYS_NUMBER;i++) {
    if (keys[i].code == code) {
      outBuffer += sprintf(outBuffer, ",\"flat\":\"%s\",\"owner\":\"%s\"",keys[i].flat, keys[i].owner);
      key = i;
    }
  }
  sprintf(outBuffer, "}");
  return key;
}

void RsGenway::getMqttCodeTopicAsText(char* topicBuffer, int key) {
  if (key>=0) {
    sprintf((char*)topicBuffer, "%s/%s",  MQTT_OPEN_CODES_TOPIC, keys[key].flat);
  } else {
    sprintf((char*)topicBuffer, "%s/Unknown",  MQTT_OPEN_CODES_TOPIC);
  }
}

bool RsGenway::handleNewEvent() {
  zeros = 0;
  index = 0;
  char code[MQTT_MAX_PACKET_SIZE];
  char topic[strlen(MQTT_OPEN_CODES_TOPIC)+30];  
  int key = getCardCodeAsText(code);
  getMqttCodeTopicAsText(topic,key);
#ifndef SKIP_MQTT
  mqtt.publish(topic, code, strlen(code),false);
#endif
  sendRawTelegram();
}

void RsGenway::sendRawTelegram() {
  char raw[2*RX_BUFFER_SIZE+1];
  char *praw = raw;
  for (uint8_t i = 0; i < RX_BUFFER_SIZE; i++) {
    praw += sprintf((char*)praw, "%02X",  buffer[i]);
  }
  #ifndef SKIP_MQTT
  mqtt.publish(MQTT_RAW_TELEGRAM, raw, 2*RX_BUFFER_SIZE, false);
  #endif
}

void RsGenway::loop() {
  if (Serial.available()) {
    const uint8_t rec = Serial.read();
    if (!detectStart(rec)) {
      if (detectEnd(rec)) {
        handleNewEvent();
      } else {
        buffer[index++] = rec;
      };
    };
  };
}



