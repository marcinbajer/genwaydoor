#pragma once
#include <stdint.h>

#define START_CHAR 0xAA
#define END_CHAR 0xFE
#define ZEROS_IN_PREAMBLE 16
#define KEY_LENGTH 10
#define RX_BUFFER_SIZE 40

class RsGenway
{
  public:
    void setup();
    void loop();
    int getCardCodeAsText(char* outBuffer);
    void getMqttCodeTopicAsText(char* topicBuffer, int i);
  private:
    bool detectStart(const uint8_t rec);
    bool detectEnd(const uint8_t rec);
    bool handleNewEvent();
    void sendRawTelegram();
    unsigned char index;
    char zeros;
    uint8_t buffer[RX_BUFFER_SIZE];
};

